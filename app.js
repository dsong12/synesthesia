'use strict';

// Framework
var express = require('express');
var app = express();

// Imports the Google Cloud client library.
const {Storage} = require('@google-cloud/storage');
// Instantiates a client. If you don't specify credentials when constructing
// the client, the client library will look for credentials in the
// environment.
const storage = new Storage();

// Parsing the file provided by the form on index.html
var formidable = require('formidable'); 
var fs = require('fs')


app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.post('/uploads', function (req, res) {
    var form = new formidable.IncomingForm();
    
    form.on('fileBegin', function(name, file) {
        file.path = __dirname + '/uploads/' + file.name
    });
    
    form.parse(req, function (err, fields, files) {
        if (err) throw err;
        if (files.picture.type == 'image/png') { console.log("true")}
        if (files.picture.type != 'image/png' && files.picture.type != 'image/jpeg') {
            fs.unlink(files.picture.path, (err) => {
                if (err) throw err;
            });
        }
        res.redirect('/');
    });
});

app.listen(process.env.PORT || 8080);

// here lies code that is useful, but not being used

// app.use(serve_static(path.join(__dirname, 'public')));

// app.get('*', (req, res) => {
//     switch(req.url) {
//         // http://localhost:8080/add_image should take an image and store it in public?
//         case '/add_image/':
//             res.sendFile(path.join(__dirname, 'public/index.html'));
//             break;
//         default:
//             res.writeHead(200, {
//                 'Content-Type': 'text/plain',
//             })
//             res.write("qwerasdf")
//             res.end();
//       }
// });

// var multer  = require('multer')
// var upload = multer({ dest: 'uploads/' })

// app.post('/upload', upload.single('avatar'), function (req, res, next) {
    // req.body contains the text fields
    // res.status(200);
    // res.set({'Content-Type': 'text/plain'});
    // res.send('thank you for your message');
//     res.redirect('add_image');
// });

// Resources
// https://stackoverflow.com/questions/15772394/how-to-upload-display-and-save-images-using-node-js-and-express
// https://shiya.io/simple-file-upload-with-express-js-and-formidable-in-node-js/
// https://cloud.google.com/docs/authentication/production TODO